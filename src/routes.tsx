import { Navigate, useRoutes } from "react-router-dom";
import DashboardLayout from "./components/Layout";
import Product from "./pages/Product";
import ShoppingCart from "./pages/ShoppingCart";
import ProductList from "./pages/ProductList";
import { Page404 } from "./pages/Page404";
import Home from "./pages/Home";
import Login from "./pages/Login";
import { useContext } from "react";
import AuthContext from "./contexts/AuthContext";
import { AuthContextType } from "./types/auth";
interface routes {
  path?: string;
  element?: JSX.Element;
  children?: routes[];
}
export const RouteList: React.FC = () => {
  const { isLoggedIn } = useContext(AuthContext) as AuthContextType;
  const islogin = localStorage.getItem("isLoggedIn");
  let routes: routes[] = [
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "*",
      element: <Navigate to="/login" replace={true} />,
    },
  ];
  if (islogin) {
    routes = [
      {
        path: "/*",
        element: <Page404 />,
      },
      {
        element: <DashboardLayout />,
        children: [
          {
            path: "/",
            element: <Home />,
          },
          {
            path: "/products/:id",
            element: <Product />,
          },
          {
            path: "/product-list",
            element: <ProductList />,
          },
          {
            path: "/shopping-cart",
            element: <ShoppingCart />,
          },
        ],
      },
    ];
  }
  return useRoutes(routes);
};
