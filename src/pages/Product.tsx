import { styled } from "@mui/material/styles";

import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useLocation } from "react-router-dom";
import { Product } from "shopping-cart-api";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { addItemToCart } from "../API/services/cart-service";
import { useRef } from "react";

const ImageContainer = styled(Grid)({
  position: "relative",
  height: "400px",
  "@media (min-width: 600px)": {
    height: "500px",
  },
});

const Image = styled("img")({
  width: "100%",
  height: "100%",
  objectFit: "cover",
});

const RightColumn = styled(Grid)({
  marginTop: "60px",
});

const ProductDescription = styled(Grid)({
  borderBottom: "1px solid #E1E8EE",
  marginBottom: "20px",
});

const ProductPrice = styled(Grid)({
  display: "flex",
  alignItems: "center",
});

export default function ProductDetail() {
  const inputRef = useRef<HTMLInputElement>(null);

  const location = useLocation();
  const { product } = location.state as { product: Product };
  return (
    <Container maxWidth="lg">
      <Grid container spacing={4}>
        <ImageContainer item xs={12} md={6}>
          <Image data-image="black" src={product.image} alt="" />
        </ImageContainer>
        <RightColumn
          item
          xs={12}
          md={6}
          container
          direction="column"
          justifyContent="space-between"
        >
          <ProductDescription item>
            <Typography
              variant="h1"
              sx={{
                fontWeight: 300,
                fontSize: "52px",
                color: "#43484D",
                letterSpacing: "-2px",
              }}
            >
              {product.name}
            </Typography>
            <Typography
              variant="body1"
              sx={{
                fontSize: "16px",
                fontWeight: 300,
                color: "#86939E",
                lineHeight: "24px",
              }}
            >
              {product.productDescription}
            </Typography>
          </ProductDescription>
          <Grid item container spacing={2}>
            <Grid item xs={6}>
              <Typography variant="h5" sx={{ fontWeight: 600 }}>
                Quantity:
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <input
                ref={inputRef}
                type="number"
                defaultValue="1"
                min="1"
                max="10"
              />
            </Grid>
          </Grid>
          <ProductPrice item container spacing={2}>
            <Grid item xs={12} md={6}>
              <Typography
                variant="h2"
                sx={{
                  fontSize: "26px",
                  fontWeight: 300,
                  color: "#43474D",
                  marginRight: "20px",
                }}
              >
                {product.price}$
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={() =>
                  addItemToCart(
                    1,
                    product.id!,
                    parseInt(inputRef.current!.value)
                  )
                }
              >
                Add to cart
              </Button>
            </Grid>
          </ProductPrice>
        </RightColumn>
      </Grid>
    </Container>
  );
}
