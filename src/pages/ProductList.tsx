import { Container, Grid } from "@mui/material";
import { getProducts } from "../API/services/product-service";
import { Product } from "shopping-cart-api";
import { useState, useEffect } from "react";
import ProductCard from "../components/ProductCard";
function ProductList() {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    //fetch products
    getProducts().then((products) => setProducts(products));
  }, []);

  return (
    <main>
      {/* Hero unit */}

      <Container sx={{ py: 8 }} maxWidth="md">
        {/* End hero unit */}
        <Grid container spacing={10}>
          {products.map((product) => (
            <Grid item xs={12} sm={6} md={4} key={product.id}>
              <ProductCard product={product} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </main>
  );
}

export default ProductList;
