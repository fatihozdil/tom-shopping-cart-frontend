import CartElements from "../components/CartElements/CartElements";
import { useEffect, useState } from "react";
import { CartDto, ItemDto } from "shopping-cart-api";
import { getCart, getCartElements } from "../API/services/cart-service";

function ShoppingCart() {
  const [total, setTotal] = useState<number>(0);

  const [cart, setCart] = useState<CartDto>();
  const [cartElements, setCartElements] = useState<ItemDto[]>([]);

  useEffect(() => {
    //fetch elements from cart
    fetchCartElements();
    //fetch cart
    fetchCart();
  }, []);
  //fetch cart elements
  const fetchCartElements = () => {
    getCartElements().then((elements) => setCartElements(elements));
  };

  //fetch cart
  const fetchCart = () => {
    getCart().then((cart) => {
      setCart(cart[0]);
      setTotal(cart[0].totalPrice!);
    });
  };

  return (
    <>
      {cart && (
        <CartElements
          cart={cart!}
          fetchCartElements={fetchCartElements}
          fetchCart={fetchCart}
          total={total}
          setTotal={setTotal}
          cartElements={cartElements}
        />
      )}
    </>
  );
}

export default ShoppingCart;
