import { useState } from "react";
import classes from "./CartElement.module.scss";
import { ItemDto } from "shopping-cart-api";
import DeleteIcon from "@mui/icons-material/Delete";
import { removeItemFromCart } from "../../../API/services/cart-service";
const CartElement = ({
  item,
  fetchCart,
  fetchCartElements,
  total,
  setTotal,
}: {
  item: ItemDto;
  fetchCart: () => void;
  fetchCartElements: () => void;
  total: number;
  setTotal: (arg: number) => void;
}) => {
  let [count, setCount] = useState(item.quantity!);

  //decrease product number and price
  const decreaseHandler = () => {
    if (count > 1) {
      setCount(count - 1);
      setTotal(total - item.product!.price!);
    }
  };
  //increase product number and price
  const increaseHandler = () => {
    setCount(count + 1);
    setTotal(total + item.product!.price!);
  };

  const removeHandler = () => {
    removeItemFromCart(item.product?.id!, item.quantity!).then((res) => {
      fetchCart();
      fetchCartElements();
    });
  };

  return (
    <div className={classes.element}>
      <div className={classes.img}>
        <img src={item.product!.image!} alt="product-info" />
      </div>
      <div className={classes.right}>
        <div className={classes.title}>
          <h3>{item.product!.name}</h3>
          <p>${item.product!.price} </p>
        </div>
        <div className={classes.subtotal}>
          <div className={classes.amount}>
            <label onClick={decreaseHandler}> &mdash; </label>

            <span> {count}</span>

            <label onClick={increaseHandler}> + </label>
          </div>
          <p>${(item.product!.price! * count).toFixed(2)}</p>
        </div>
        <div className={classes.functions}>
          <div onClick={removeHandler} className={classes.remove}>
            <DeleteIcon />
            <p>Remove</p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CartElement;
