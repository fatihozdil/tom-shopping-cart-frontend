import classes from "./CartElements.module.scss";
import CartElement from "./CartElement/CartElement";
import { CartDto, Coupon, ItemDto } from "shopping-cart-api";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
} from "@mui/material";
import { useEffect, useState } from "react";
import {
  addCoupon,
  getCoupons,
  removeCouponFromCart,
} from "../../API/services/cart-service";
const CartElements = ({
  cart,
  fetchCart,
  fetchCartElements,
  cartElements,
  total,
  setTotal,
}: {
  cart: CartDto;
  fetchCart: () => void;
  fetchCartElements: () => void;
  cartElements: ItemDto[];
  total: number;
  setTotal: (arg: number) => void;
}) => {
  const [selectedCoupon, setSelectedCoupon] = useState<number>(
    cart.coupon ? cart.coupon.id! : 0
  );
  const [coupons, setCoupons] = useState<Coupon[]>([]);

  const elements = cartElements.map((el, i) => {
    return (
      <CartElement
        fetchCartElements={fetchCartElements}
        fetchCart={fetchCart}
        key={i}
        item={el}
        total={total}
        setTotal={setTotal}
      />
    );
  });

  const applyCoupon = (event: SelectChangeEvent<number>) => {
    removeCouponFromCart().then((res) => {
      const value = event.target.value as number;

      addCoupon(value as number).then((res) => {
        fetchCart();
        setSelectedCoupon(value);
      });
    });
  };

  useEffect(() => {
    getCoupons().then((coupons) => {
      setCoupons(coupons);
    });
  }, []);
  return (
    <div className={classes.elements}>
      {elements}

      <div className={classes.bottom}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Coupon</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={selectedCoupon}
            label="coupon"
            onChange={applyCoupon}
          >
            {coupons.map((coupon, i) => {
              return (
                <MenuItem key={coupon.id} value={coupon.id}>
                  {coupon.couponDescription}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <div className={classes.price}>
          <div className={classes.subtotal}>
            <p> Items Total</p>
            <h4> ${total!.toFixed(2)}</h4>
          </div>

          <div className={classes.total}>
            <p>Coupon Applied</p>
            <h3> ${cart.finalValue!.toFixed(2)}</h3>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CartElements;
