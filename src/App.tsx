import { CssBaseline } from "@mui/material";
import {
  StyledEngineProvider,
  ThemeProvider,
  createTheme,
} from "@mui/material/styles";
import { BrowserRouter } from "react-router-dom";
import { RouteList } from "./routes";
import AuthContext from "./contexts/AuthContext";
import { useState } from "react";

export const App = (): JSX.Element => {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  const theme = createTheme({
    palette: {
      mode: "light",
    },
  });

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
          <CssBaseline />
          <BrowserRouter>
            <RouteList />
          </BrowserRouter>
        </AuthContext.Provider>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default App;
