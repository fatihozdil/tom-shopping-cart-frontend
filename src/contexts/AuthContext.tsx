import React from "react";
import { AuthContextType } from "../types/auth";

const AuthContext = React.createContext<AuthContextType | null>(null);

export default AuthContext;
