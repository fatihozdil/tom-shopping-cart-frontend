import { Configuration } from "shopping-cart-api";
import { HOST_URL } from "./settings";

const baseURL = HOST_URL;

export let config = new Configuration({
  basePath: baseURL,
});
