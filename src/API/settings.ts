//store api urls as variable
let DEBUG = true;
let HOST_URL = "";
let SOCKET_URL = "";
if (DEBUG) {
  HOST_URL = "http://127.0.0.1:8080";
  SOCKET_URL = "ws://127.0.0.1:8080";
}

export { HOST_URL, SOCKET_URL };
