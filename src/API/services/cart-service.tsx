import { CartControllerApi, CouponControllerApi } from "shopping-cart-api";

const cartApi = new CartControllerApi();
const couponApi = new CouponControllerApi();
export const addItemToCart = (
  cartId: number,
  productId: number,
  quantity: number
) => {
  cartApi.addItemToCart(cartId, productId, quantity).catch((err) => {
    console.log(err);
  });
};

export const getCartElements = async () => {
  const response = await cartApi.getItems(1);
  return response.data;
};

export const getCart = async () => {
  const response = await cartApi.getCarts();
  return response.data;
};

export const getCoupons = async () => {
  const response = await couponApi.findAllCoupon();
  return response.data;
};

export const addCoupon = async (couponId: number) => {
  const response = await cartApi.applyCouponToCart(1, couponId);
  return response.data;
};
export const removeItemFromCart = async (itemId: number, quantity: number) => {
  const response = await cartApi.removeItemFromCart(1, itemId, quantity);
  return response.data;
};

export const removeCouponFromCart = async () => {
  const response = await cartApi.removeCouponFromCart(1);
  return response.data;
};
