import { ProductControllerApi } from "shopping-cart-api";

const productApi = new ProductControllerApi();

export const getProducts = async () => {
  const products = await productApi.getAllProduct();

  return products.data;
};
