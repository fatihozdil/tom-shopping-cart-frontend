# Initialize Project

- clone or download repo
- run

```
    npm install ; npm start
```

# Live Version

- note that since backend is not served, it cannot communicate with backend.  
  [live link](https://tom-shopping-cart-frontend.vercel.app/)
- How I implemented Open api. This tutorial created by me
  [video link](https://youtu.be/LsjTIZWku4o?t=133)

# Project Pages

## Login Page

![Login Page](/src/assets/loginpage.png "Login Page")

## Product List Page

![Product List Page](/src/assets/Product%20List.png "Product List Page")

## Product Detail Page

![Product Detail Page](/src/assets/product%20detail.png "Product Detail Page")

## Shopping Cart Page

![Shopping Cart Page](/src/assets/shopping%20cart.png "Shopping Cart Page")
